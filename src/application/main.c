/**
 * @file main.c
 * @brief
 * @author Постнов В.М.
 * @date 6 июн. 2020 г.
 * @addtogroup
 * @{
*/

#include "config.h"

#include <math.h>
#include <stddef.h>

#include "btns.h"
#include "drivers/tick_timer.h"
#include "led.h"
#include "led_cube.h"
#if (FEATURE_ENABLE_MEMS)
  #include "mems/mems.h"
#endif
#if (FEATURE_ENABLE_MIC)
  #include "mic.h"
#endif
#include "drivers/tick_timer.h"
#include "snake.h"

// ======================================= Definition ==============================================

// ======================================= Declaration =============================================

led_cube_t led_cube;
snake_t snake;

// ======================================== Implementation =========================================

int main(void)
{
  SystemCoreClockUpdate();

  tick_timer_init(NULL);

  uint32_t timestamp_snake;
  START_TIMER(timestamp_snake);
  uint32_t timestamp_1ms;
  START_TIMER(timestamp_1ms);
  uint32_t timestamp_tilt;
  START_TIMER(timestamp_tilt);

  led_init();
  btns_init();

  #if (FEATURE_ENABLE_MEMS)
  mems_init();
  #endif
  #if (FEATURE_ENABLE_MIC)
  mic_init();
  #endif

  led_cube_init(&led_cube);
//  led_cube_test(&led_cube);
  led_cube_start_timer(&led_cube);

  snake_init(&snake, &led_cube);

  while(1)
  {
    if (CHECK_TIMER(timestamp_1ms, 1))
    {
      START_TIMER(timestamp_1ms);
      btns_process();
      btn_event_t last_event;
      btns_get_and_clear_last_event(&last_event);
      if (last_event.type != BTN_EVENT_T__NONE)
      {
      }
    }

    if (CHECK_TIMER(timestamp_snake, 150))
    {
      START_TIMER(timestamp_snake);
      snake_next_step(&snake);
    }

    if (CHECK_TIMER(timestamp_tilt, 100))
    {
      START_TIMER(timestamp_tilt);
//      mems_angles_t mems_angles;
//      mems_get_angles(&mems_angles);
//      for (uint8_t i = 0; i < 4; i++)
//      {
//        int8_t dz = (int8_t)roundf(mems_angles.y * (4 - i) / mems_angles.z);
//        led_cube_set_cell(&led_cube, 0, 0 + i, 4 + dz, LED_CUBE_MAX_BRIGHTNESS, LED_CUBE_CELL_ON_MODE__AUTO_OFF);
//        led_cube_set_cell(&led_cube, 0, 7 - i, 4 - dz, LED_CUBE_MAX_BRIGHTNESS, LED_CUBE_CELL_ON_MODE__AUTO_OFF);
//      }
    }
  }
}

/** @} */
