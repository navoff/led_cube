/**
 * @file config_led_cube_8x8x8_rev001.h
 * @brief
 * @author Vladimir Postnov
 * @date 24/07/2020
 * @addtogroup
 * @{
*/

#ifndef CONFIG_LED_CUBE_8X8X8_REV001_H_
#define CONFIG_LED_CUBE_8X8X8_REV001_H_

#ifdef __cplusplus
extern "C" {
#endif

// ========================================= Definition ============================================

/**
 * @brief Модификации плат
 * @addtogroup DEVICE_HW__MODIFICATIONS
 * @{
 */
  /// @note Базовая модификация без изменений
  #define HW_MOD_0                              0
/** @} */

/// Выбранная модификация платы
#define HW_MODIFICATION                         HW_MOD_0

/// Смещение таблицы прерываний
#define VECT_TAB_OFFSET                         0x00000

#define RANDOM_ADC_NUMBER                       1
#define RANDOM_ADC_DMA_NUMBER                   2
#define RANDOM_ADC_DMA_STREAM_IDX               0
#define RANDOM_ADC_DMA_CHANNEL_IDX              0

#define LED_CUBE_X_SIZE                         8
#define LED_CUBE_Y_SIZE                         8
#define LED_CUBE_Z_SIZE                         8
#define LED_CUBE_TIM_NUMBER                     1
#define LED_CUBE_TIM_IRQ                        TIM1_UP_TIM10_IRQn
#define LED_CUBE_TIMER_PRESCALER                0
#define LED_CUBE_TIMER_PERIOD                   4200

#define LED_GPIO_IDX                            6
#define LED_PIN_IDX                             11

#define BTN_AMOUNT                              1
#define BTN_GPIO_IDX                            4
#define BTN_PIN_IDX                             0

#define FEATURE_ENABLE_MEMS                     1
#define MEMS_AXES_AMOUNT                        3

#define FEATURE_ENABLE_LIS331                   1

#define FEATURE_ENABLE_MIC                      1

#if (FEATURE_ENABLE_LIS331)
/**
 * @defgroup LIS331_HW_SETTINGS
 *
 * @{
 */
  #define LIS331_SPI_NUMBER                     1
  #define LIS331_SPI_FREQUENCY                  1000000U

  #define LIS331_SPI_CLK_GPIO_IDX               1
  #define LIS331_SPI_CLK_PIN_IDX                3

  #define LIS331_SPI_MISO_GPIO_IDX              1
  #define LIS331_SPI_MISO_PIN_IDX               4

  #define LIS331_SPI_MOSI_GPIO_IDX              1
  #define LIS331_SPI_MOSI_PIN_IDX               5

  #define LIS331_SPI_CS_GPIO_IDX                6
  #define LIS331_SPI_CS_PIN_IDX                 12

  #define LIS331_INT1_GPIO_IDX                  6
  #define LIS331_INT1_PIN_IDX                   14
/** @} */
#endif // #if (FEATURE_ENABLE_LIS331)

#if (FEATURE_ENABLE_MIC)
  #define MIC_I2S_NUMBER                        3

  #define MIC_I2S_CLK_GPIO_IDX                  2
  #define MIC_I2S_CLK_PIN_IDX                   10

  #define MIC_I2S_SD_GPIO_IDX                   2
  #define MIC_I2S_SD_PIN_IDX                    12

  #define MIC_I2S_WS_GPIO_IDX                   0
  #define MIC_I2S_WS_PIN_IDX                    15

  #define MIC_I2S_DMA_NUMBER                    1
  #define MIC_I2S_DMA_STREAM_IDX                0
  #define MIC_I2S_DMA_CHANNEL_IDX               0
#endif

#define FEATURE_ENABLE_UART1                    0
#define FEATURE_ENABLE_UART2                    0
#define FEATURE_ENABLE_UART3                    0
#define FEATURE_ENABLE_UART4                    0
#define FEATURE_ENABLE_UART5                    0
#define FEATURE_ENABLE_UART6                    0
#define FEATURE_ENABLE_UART7                    0
#define FEATURE_ENABLE_UART8                    0

#define FEATURE_ENABLE_ADC1                     1
#define FEATURE_ENABLE_ADC2                     0
#define FEATURE_ENABLE_ADC3                     0
#define FEATURE_ENABLE_ADC4                     0

#define FEATURE_ENABLE_DMA1                     1
#define FEATURE_ENABLE_DMA2                     1

#define FEATURE_ENABLE_SPI1                     1
#define FEATURE_ENABLE_SPI2                     0
#define FEATURE_ENABLE_SPI3                     1
#define FEATURE_ENABLE_SPI4                     0

#define FEATURE_ENABLE_I2S2                     0
#define FEATURE_ENABLE_I2S3                     1


// ========================================= Declaration ===========================================


#ifdef __cplusplus
}
#endif

#endif /* CONFIG_LED_CUBE_8X8X8_REV001_H_ */

/** @} */
