/**
 * @file config.h
 * @brief
 * @author Постнов В.М.
 * @date 27 янв. 2017 г.
 * @addtogroup
 * @{
*/

#ifndef CONFIG_H_
#define CONFIG_H_

// ======================================= Definition ==============================================

/**
 * @defgroup HW_TYPE_LIST
 * Типы (идентификаторы) плат.
 * @{
 */
  /// Отладочная плата NUCLEO-F103RB
  #define HW_TYPE__LED_CUBE_NUCLEO_F103RB           1

  /// Плата LedCube4x4x4 rev0001
  #define HW_TYPE__LED_CUBE_4X4X4_REV001            2

  /// Плата LedCube8x8x8 rev0001
  #define HW_TYPE__LED_CUBE_8X8X8_REV001            3

/** @} */

/**
 * @defgroup FW_TYPE_LIST
 * Тип (идентификаторы) прошивок
 * @{
 */
  /// Загрузчик
  #define FW_TYPE__DEFAULT                        0x00000001
/** @} */

// Тип платы выбирать здесь:
//#define HW_TYPE                                   HW_TYPE__LED_CUBE_NUCLEO_F103RB
//#define HW_TYPE                                   HW_TYPE__LED_CUBE_4X4X4_REV001
//#define HW_TYPE                                   HW_TYPE__LED_CUBE_8X8X8_REV001

// Тип прошивки выбирать здесь:
#define FW_TYPE                                   FW_TYPE__DEFAULT


#if defined(DEBUG)
  #define USE_CHECK_PARAMS_EN                 1
#elif defined(RELEASE)
  #define USE_CHECK_PARAMS_EN                 0
#else
  #error В настройках проекта нужно указать тип сборки для данной цели компиляции!
#endif

/**
 * @brief Версия прошивки
 * @addtogroup FW_VERSIONS
 * @{
 */
  /// Версия прошивки
  #define DEVICE_INFO__FW_VERSION_MAJOR       1

  /// Подверсия прошивки
  #define DEVICE_INFO__FW_VERSION_MINOR       0

  /// Версия сборки
  #define DEVICE_INFO__FW_BUILD               0x1
/** @} */

#if (HW_TYPE == HW_TYPE__LED_CUBE_NUCLEO_F103RB)
  #include "config_led_cube_nucleo_f103rb.h"
  #include "stm32f10x.h"
  #define STM32F1XX

#elif (HW_TYPE == HW_TYPE__LED_CUBE_4X4X4_REV001)
  #include "config_led_cube_4x4x4_rev001.h"
  #include "stm32f30x.h"
  #define STM32F30X

#elif (HW_TYPE == HW_TYPE__LED_CUBE_8X8X8_REV001)
  #include "config_led_cube_8x8x8_rev001.h"
  #include "stm32f4xx.h"
  #define STM32F4XX

#endif

#include "utils/hal_config.h"

// ======================================= Declaration =============================================

#endif /* CONFIG_H_ */

/** @} */
