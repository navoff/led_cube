/**
 * @file config_retro_clock_rev001.h
 * @author Постнов В.М.
 * @date 9 мая 2018 г.
 * @brief
 * @addtogroup
 * @{
*/

#ifndef CONFIG_RETRO_CLOCK_REV001_H_
#define CONFIG_RETRO_CLOCK_REV001_H_

// ================================ Definition ================================

/**
 * @brief Модификации плат
 * @addtogroup DEVICE_HW__MODIFICATIONS
 * @{
 */
  /// @note Базовая модификация без изменений
  #define HW_MOD_0                              0
/** @} */

/// Выбранная модификация платы
#define HW_MODIFICATION                         HW_MOD_0

/// Смещение таблицы прерываний
#define VECT_TAB_OFFSET                         0x00000

#define RANDOM_ADC_NUMBER                       1
#define RANDOM_ADC_DMA_NUMBER                   1
#define RANDOM_ADC_DMA_CHANNEL_NUMBER           1

#define LED_CUBE_X_SIZE                         4
#define LED_CUBE_Y_SIZE                         4
#define LED_CUBE_Z_SIZE                         4
#define LED_CUBE_TIM_NUMBER                     1
#define LED_CUBE_TIM_IRQ                        TIM1_UP_IRQn
#define LED_CUBE_TIMER_PRESCALER                0
#define LED_CUBE_TIMER_PERIOD                   3600

#define LED_GPIO_IDX                            0
#define LED_PIN_IDX                             5

#define BTN_AMOUNT                              1
#define BTN_GPIO_IDX                            2
#define BTN_PIN_IDX                             13

#define FEATURE_ENABLE_UART1                    0
#define FEATURE_ENABLE_UART2                    0
#define FEATURE_ENABLE_UART3                    0
#define FEATURE_ENABLE_UART4                    0
#define FEATURE_ENABLE_UART5                    0
#define FEATURE_ENABLE_UART6                    0
#define FEATURE_ENABLE_UART7                    0
#define FEATURE_ENABLE_UART8                    0

#define FEATURE_ENABLE_ADC1                     1
#define FEATURE_ENABLE_ADC2                     0
#define FEATURE_ENABLE_ADC3                     0
#define FEATURE_ENABLE_ADC4                     0

#define FEATURE_ENABLE_DMA1                     1
#define FEATURE_ENABLE_DMA2                     0

#define FEATURE_ENABLE_I2S2                     0
#define FEATURE_ENABLE_I2S3                     0

// =============================== Declaration ================================


#endif /* CONFIG_RETRO_CLOCK_REV001_H_ */

/** @} */
