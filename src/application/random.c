/**
 * @file random.cpp
 * @brief
 * @author Vladimir Postnov
 * @date 11/06/2020
 * @addtogroup
 * @{
 */

#include "drivers/adc.h"
#include "drivers/dma.h"
#include "drivers/gpio.h"

#include "random.h"

// ======================================= Definition ==============================================

#define RANDOM_ADC_DMA_BUFFER_SIZE            32

static uint16_t random_adc_dma_buffer[RANDOM_ADC_DMA_BUFFER_SIZE];

// ======================================== Implementation =========================================

void random_init(void)
{
  #if defined(STM32F1XX)
  const adc_hw_item_t * adc_hw = &adc_hw_items[RANDOM_ADC_NUMBER - 1];
  const dma_channel_hw_item_t *dma_channel_hw =
      &dma_channel_hw_items[RANDOM_ADC_DMA_NUMBER - 1][RANDOM_ADC_DMA_CHANNEL_NUMBER - 1];

  adc_rcc_cmd(RANDOM_ADC_NUMBER, ENABLE);
  dma_rcc_cmd(RANDOM_ADC_DMA_NUMBER, ENABLE);

  DMA_InitTypeDef dma_init_struct;
  dma_init_struct.DMA_PeripheralBaseAddr = (uint32_t)&adc_hw->adc->DR;
  dma_init_struct.DMA_MemoryBaseAddr = (uint32_t)random_adc_dma_buffer;
  dma_init_struct.DMA_DIR = DMA_DIR_PeripheralSRC;
  dma_init_struct.DMA_BufferSize = RANDOM_ADC_DMA_BUFFER_SIZE;
  dma_init_struct.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
  dma_init_struct.DMA_MemoryInc = DMA_MemoryInc_Enable;
  dma_init_struct.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
  dma_init_struct.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
  dma_init_struct.DMA_Mode = DMA_Mode_Circular;
  dma_init_struct.DMA_Priority = DMA_Priority_Medium;
  dma_init_struct.DMA_M2M = DMA_M2M_Disable;
  DMA_Init(dma_channel_hw->channel, &dma_init_struct);
  DMA_Cmd(dma_channel_hw->channel, ENABLE);

  ADC_InitTypeDef adc_init_struct;
  ADC_StructInit(&adc_init_struct);
  adc_init_struct.ADC_Mode = ADC_Mode_Independent;
  adc_init_struct.ADC_ScanConvMode = ENABLE;
  adc_init_struct.ADC_ContinuousConvMode = ENABLE;
  adc_init_struct.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
  adc_init_struct.ADC_DataAlign = ADC_DataAlign_Right;
  adc_init_struct.ADC_NbrOfChannel = 1;
  ADC_Init(adc_hw->adc, &adc_init_struct);

  ADC_TempSensorVrefintCmd(ENABLE);
  ADC_RegularChannelConfig(adc_hw->adc, ADC_Channel_TempSensor, 1, ADC_SampleTime_55Cycles5);
  ADC_DMACmd(adc_hw->adc, ENABLE);
  ADC_Cmd(adc_hw->adc, ENABLE);
  ADC_SoftwareStartConvCmd(adc_hw->adc, ENABLE);

  #elif defined(STM32F30X)
  const adc_hw_item_t *adc_hw = &adc_hw_items[RANDOM_ADC_NUMBER - 1];
  const dma_channel_hw_item_t *dma_channel_hw =
      &dma_channel_hw_items[RANDOM_ADC_DMA_NUMBER - 1][RANDOM_ADC_DMA_CHANNEL_NUMBER - 1];

  adc_rcc_cmd(RANDOM_ADC_NUMBER, ENABLE);
  dma_rcc_cmd(RANDOM_ADC_DMA_NUMBER, ENABLE);

  DMA_InitTypeDef dma_init_struct;
  dma_init_struct.DMA_PeripheralBaseAddr = (uint32_t)&adc_hw->adc->DR;
  dma_init_struct.DMA_MemoryBaseAddr = (uint32_t)random_adc_dma_buffer;
  dma_init_struct.DMA_DIR = DMA_DIR_PeripheralSRC;
  dma_init_struct.DMA_BufferSize = RANDOM_ADC_DMA_BUFFER_SIZE;
  dma_init_struct.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
  dma_init_struct.DMA_MemoryInc = DMA_MemoryInc_Enable;
  dma_init_struct.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
  dma_init_struct.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
  dma_init_struct.DMA_Mode = DMA_Mode_Circular;
  dma_init_struct.DMA_Priority = DMA_Priority_Medium;
  dma_init_struct.DMA_M2M = DMA_M2M_Disable;
  DMA_Init(dma_channel_hw->channel, &dma_init_struct);
  DMA_Cmd(dma_channel_hw->channel, ENABLE);

  RCC_ADCCLKConfig(RCC_ADC12PLLCLK_Div4);

  ADC_CommonInitTypeDef adc_common_init_struct;
  ADC_CommonStructInit(&adc_common_init_struct);
  adc_common_init_struct.ADC_Mode = ADC_Mode_Independent;
  adc_common_init_struct.ADC_Clock = ADC_Clock_AsynClkMode;
  adc_common_init_struct.ADC_DMAAccessMode = ADC_DMAAccessMode_1;
  adc_common_init_struct.ADC_DMAMode = ADC_DMAMode_Circular;
  adc_common_init_struct.ADC_TwoSamplingDelay = 0;
  ADC_CommonInit(adc_hw->adc, &adc_common_init_struct);

  ADC_InitTypeDef adc_init_struct;
  ADC_StructInit(&adc_init_struct);
  adc_init_struct.ADC_ContinuousConvMode = ADC_ContinuousConvMode_Enable;
  adc_init_struct.ADC_ExternalTrigConvEvent = ADC_ExternalTrigConvEvent_0;
  adc_init_struct.ADC_ExternalTrigEventEdge = ADC_ExternalTrigEventEdge_None;
  adc_init_struct.ADC_Resolution = ADC_Resolution_12b;
  adc_init_struct.ADC_DataAlign = ADC_DataAlign_Right;
  adc_init_struct.ADC_OverrunMode = ADC_OverrunMode_Disable;
  adc_init_struct.ADC_AutoInjMode = ADC_AutoInjec_Disable;
  adc_init_struct.ADC_NbrOfRegChannel = 1;
  ADC_Init(adc_hw->adc, &adc_init_struct);

  ADC_TempSensorCmd(adc_hw->adc, ENABLE);
  ADC_RegularChannelConfig(adc_hw->adc, ADC_Channel_TempSensor, 1, ADC_SampleTime_61Cycles5);

  ADC_Cmd(adc_hw->adc, ENABLE);
  while(!ADC_GetFlagStatus(adc_hw->adc, ADC_FLAG_RDY));

  ADC_DMACmd(adc_hw->adc, ENABLE);
  ADC_DMAConfig(adc_hw->adc, ADC_DMAMode_Circular);
  ADC_StartConversion(adc_hw->adc);

  #elif defined(STM32F4XX)
  const adc_hw_item_t *adc_hw = &adc_hw_items[RANDOM_ADC_NUMBER - 1];
  const dma_steam_hw_item_t *dma_stream_hw =
      &dma_stream_hw_items[RANDOM_ADC_DMA_NUMBER - 1][RANDOM_ADC_DMA_STREAM_IDX];

  adc_rcc_cmd(RANDOM_ADC_NUMBER, ENABLE);
  dma_rcc_cmd(RANDOM_ADC_DMA_NUMBER, ENABLE);

  DMA_InitTypeDef dma_init_struct;
  dma_init_struct.DMA_Channel = dma_channels[RANDOM_ADC_DMA_CHANNEL_IDX];
  dma_init_struct.DMA_PeripheralBaseAddr = (uint32_t)&adc_hw->adc->DR;
  dma_init_struct.DMA_Memory0BaseAddr = (uint32_t)random_adc_dma_buffer;
  dma_init_struct.DMA_DIR = DMA_DIR_PeripheralToMemory;
  dma_init_struct.DMA_BufferSize = RANDOM_ADC_DMA_BUFFER_SIZE;
  dma_init_struct.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
  dma_init_struct.DMA_MemoryInc = DMA_MemoryInc_Enable;
  dma_init_struct.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
  dma_init_struct.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
  dma_init_struct.DMA_Mode = DMA_Mode_Circular;
  dma_init_struct.DMA_Priority = DMA_Priority_Medium;
  dma_init_struct.DMA_FIFOMode = DMA_FIFOMode_Disable;
  dma_init_struct.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;
  dma_init_struct.DMA_MemoryBurst = DMA_MemoryBurst_Single;
  dma_init_struct.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
  DMA_Init(dma_stream_hw->stream, &dma_init_struct);
  DMA_Cmd(dma_stream_hw->stream, ENABLE);

  ADC_CommonInitTypeDef adc_common_init_struct;
  ADC_CommonStructInit(&adc_common_init_struct);
  adc_common_init_struct.ADC_Mode = ADC_Mode_Independent;
  adc_common_init_struct.ADC_Prescaler = ADC_Prescaler_Div8;
  adc_common_init_struct.ADC_DMAAccessMode = ADC_DMAAccessMode_1;
  adc_common_init_struct.ADC_TwoSamplingDelay = 0;
  ADC_CommonInit(&adc_common_init_struct);

  ADC_InitTypeDef adc_init_struct;
  ADC_StructInit(&adc_init_struct);
  adc_init_struct.ADC_Resolution = ADC_Resolution_12b;
  adc_init_struct.ADC_ScanConvMode = DISABLE;
  adc_init_struct.ADC_ContinuousConvMode = ENABLE;
  adc_init_struct.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;
  adc_init_struct.ADC_ExternalTrigConv = ADC_ExternalTrigConv_T1_CC1;
  adc_init_struct.ADC_DataAlign = ADC_DataAlign_Right;
  adc_init_struct.ADC_NbrOfConversion = 1;
  ADC_Init(adc_hw->adc, &adc_init_struct);

  ADC_TempSensorVrefintCmd(ENABLE);
  ADC_RegularChannelConfig(adc_hw->adc, ADC_Channel_TempSensor, 1, ADC_SampleTime_56Cycles);

  ADC_DMARequestAfterLastTransferCmd(adc_hw->adc, ENABLE);
  ADC_DMACmd(adc_hw->adc, ENABLE);
  ADC_Cmd(adc_hw->adc, ENABLE);

  ADC_SoftwareStartConv(adc_hw->adc);

  #else
    #error HW not defined
  #endif
}

uint32_t random_get_next(void)
{
  uint32_t result = 0;
  for (int i = 0; i < RANDOM_ADC_DMA_BUFFER_SIZE; ++i)
  {
    result <<= 1;
    result |= random_adc_dma_buffer[i] & 0x1;
  }

  return result;
}
/** @} */
