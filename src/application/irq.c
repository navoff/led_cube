/**
 * @file irq.c
 * @brief
 * @author Vladimir Postnov
 * @date Jul 22, 2020
 * @addtogroup
 * @{
*/

#include "config.h"

#include "led_cube.h"
#if defined(FEATURE_ENABLE_LIS331)
  #include "mems/lis331.h"
#endif

// ========================================= Definition ============================================

// ========================================= Declaration ===========================================

extern led_cube_t led_cube;

// ======================================== Implementation =========================================


#if (HW_TYPE == HW_TYPE__LED_CUBE_NUCLEO_F103RB)
void TIM1_UP_IRQHandler(void)
{
  TIM_ClearITPendingBit(TIM1, TIM_IT_Update);

  led_cube_irq_handler(&led_cube);
}
#elif (HW_TYPE == HW_TYPE__LED_CUBE_4X4X4_REV001)
void TIM1_UP_TIM16_IRQHandler(void)
{
  TIM_ClearITPendingBit(TIM1, TIM_IT_Update);

  led_cube_irq_handler(&led_cube);
}
#elif (HW_TYPE == HW_TYPE__LED_CUBE_8X8X8_REV001)
void TIM1_UP_TIM10_IRQHandler(void)
{
  TIM_ClearITPendingBit(TIM1, TIM_IT_Update);

  led_cube_irq_handler(&led_cube);
}

void EXTI15_10_IRQHandler(void)
{
  #if (FEATURE_ENABLE_LIS331)
  lis331_drdy_handler();
  #endif
}


#endif

/** @} */
