/**
 * @file snake.c
 * @brief
 * @author Vladimir Postnov
 * @date 10/06/2020
 * @addtogroup
 * @{
 */

#include <string.h>

#include "random.h"

#include "snake.h"

// ======================================= Definition ==============================================

#define SNAKE_ROTATION_MATRIX_SIZE              7

const snake_rotation_matrix_t snake_rotation_matrix[SNAKE_ROTATION_MATRIX_SIZE] =
{
    { // без вращений
        {  1,  0,  0 },
        {  0,  1,  0 },
        {  0,  0,  1 },
    },
    { // вокруг OX на +90 градусов
        {  1,  0,  0 },
        {  0,  0, -1 },
        {  0,  1,  0 },
    },
    { // вокруг OX на -90 градусов
        {  1,  0,  0 },
        {  0,  0,  1 },
        {  0, -1,  0 },
    },
    { // вокруг OY на 90 градусов
        {  0,  0,  1 },
        {  0,  1,  0 },
        { -1,  0,  0 },
    },
    { // вокруг OY на -90 градусов
        {  0,  0, -1 },
        {  0,  1,  0 },
        {  1,  0,  0 },
    },
    { // вокруг OZ на +90 градусов
        {  0, -1,  0 },
        {  1,  0,  0 },
        {  0,  0,  1 },
    },
    { // вокруг OZ на -90 градусов
        {  0,  1,  0 },
        { -1,  0,  0 },
        {  0,  0,  1 },
    },
};

bool snake_is_position_inside_cube(const snake_position_t *position);
snake_speed_t snake_rotate_speed(const snake_speed_t* speed, const snake_rotation_matrix_t* matrix);

void snake_append_node(snake_t *snake, uint8_t x, uint8_t y, uint8_t z, uint8_t b);
void snake_move_to_node(snake_t *snake, snake_node_t *new_head);
bool snake_is_position_free(snake_t *snake, const snake_position_t *position);
bool snake_is_adjacent_position_free(snake_t *snake, const snake_position_t *position);
void snake_get_next_position(snake_t *snake, snake_speed_t* speed, snake_position_t *next_position);

// ======================================== Implementation =========================================

void snake_init(snake_t *snake, led_cube_t *led_cube)
{
  random_init();
  snake->led_cube = led_cube;
  snake_set_default(snake);
}

void snake_set_default(snake_t *snake)
{
  snake->length = 0;
  snake_append_node(snake, 0, 0, 0, 20);

  snake->speed.x = 0;
  snake->speed.y = 1;
  snake->speed.z = 0;

  for (uint16_t i = 0; i < snake->length; i++)
  {
    led_cube_set_cell(
        snake->led_cube,
        snake->nodes[i].p.x,
        snake->nodes[i].p.y,
        snake->nodes[i].p.z,
        snake->nodes[i].b,
        LED_CUBE_CELL_ON_MODE__AUTO_OFF);
  }
}

void snake_append_node(snake_t *snake, uint8_t x, uint8_t y, uint8_t z, uint8_t b)
{
  if (snake->length >= SNAKE_MAK_LENGTH)
  {
    return;
  }

  // todo проверить корректность координат новой головы
  for (uint16_t i = snake->length; i > 0; --i)
  {
    memcpy(&snake->nodes[i], &snake->nodes[i - 1], sizeof(snake_node_t));
  }
  snake->nodes[0].p.x = x;
  snake->nodes[0].p.y = y;
  snake->nodes[0].p.z = z;
  snake->nodes[0].b = b;
  snake->length++;
}

void snake_move_to_node(snake_t *snake, snake_node_t* new_head)
{
  if (snake->length == 0)
  {
    return;
  }

  for (uint16_t i = snake->length - 1; i > 0; --i)
  {
    memcpy(&snake->nodes[i].p, &snake->nodes[i - 1].p, sizeof(snake_position_t));
  }
  memcpy(&snake->nodes[0], new_head, sizeof(snake_node_t));

  snake_node_t *head_node = &snake->nodes[0];
  led_cube_set_cell(
      snake->led_cube,
      head_node->p.x, head_node->p.y, head_node->p.z, head_node->b,
      LED_CUBE_CELL_ON_MODE__AUTO_OFF);
}

bool snake_is_position_inside_cube(const snake_position_t *position)
{
  if ((position->x < 0) || (position->y < 0) || (position->z < 0))
  {
    return false;
  }

  if ((position->x >= LED_CUBE_X_SIZE) ||
      (position->y >= LED_CUBE_Y_SIZE) ||
      (position->z >= LED_CUBE_Z_SIZE))
  {
    return false;
  }

  return true;
}

bool snake_is_position_free(snake_t *snake, const snake_position_t *position)
{
  for (int i = 0; i < snake->length; i++)
  {
    if ((snake->nodes[i].p.x == position->x) &&
        (snake->nodes[i].p.y == position->y) &&
        (snake->nodes[i].p.z == position->z))
    {
      return false;
    }
  }

  return true;
}

bool snake_is_adjacent_position_free(snake_t *snake, const snake_position_t *pos)
{
  int idx = 0;

  if ((pos->x > 0) &&
      (!led_cube_is_node_off(snake->led_cube, pos->x - 1, pos->y, pos->z)))
  {
    idx++;
  }
  if ((pos->y > 0) &&
      (!led_cube_is_node_off(snake->led_cube, pos->x, pos->y - 1, pos->z)))
  {
    idx++;
  }
  if ((pos->z > 0) &&
      (!led_cube_is_node_off(snake->led_cube, pos->x, pos->y, pos->z - 1)))
  {
    idx++;
  }

  if ((pos->x < (LED_CUBE_X_SIZE - 1)) &&
      (!led_cube_is_node_off(snake->led_cube, pos->x + 1, pos->y, pos->z)))
  {
    idx++;
  }
  if ((pos->y < (LED_CUBE_Y_SIZE - 1)) &&
      (!led_cube_is_node_off(snake->led_cube, pos->x, pos->y + 1, pos->z)))
  {
    idx++;
  }

  if ((pos->z < (LED_CUBE_Z_SIZE - 1)) &&
      (!led_cube_is_node_off(snake->led_cube, pos->x, pos->y, pos->z + 1)))
  {
    idx++;
  }

  return (idx <= 1);
}

snake_speed_t snake_rotate_speed(const snake_speed_t* speed, const snake_rotation_matrix_t* matrix)
{
  snake_speed_t result = {};

  for (int i = 0; i < SNAKE_SPACE_DIMENSION; ++i)
  {
    int8_t sum = 0;
    for (int k = 0; k < SNAKE_SPACE_DIMENSION; ++k)
    {
      sum += (*matrix)[i][k] * speed->s[k];
    }
    result.s[i] = sum;
  }

  return result;
}

void snake_get_next_position(snake_t *snake, snake_speed_t* speed, snake_position_t *next_position)
{
  next_position->x = snake->nodes[0].p.x + speed->x;
  next_position->y = snake->nodes[0].p.y + speed->y;
  next_position->z = snake->nodes[0].p.z + speed->z;
}

void snake_next_step(snake_t *snake)
{
  int idx = random_get_next() % SNAKE_ROTATION_MATRIX_SIZE;
  for (uint8_t i = 0; i < SNAKE_ROTATION_MATRIX_SIZE; ++i)
  {
    snake_speed_t new_speed = snake_rotate_speed(&snake->speed, &snake_rotation_matrix[idx]);
    snake_position_t new_pos;
    snake_get_next_position(snake, &new_speed, &new_pos);
    if (snake_is_position_inside_cube(&new_pos) &&
        snake_is_position_free(snake, &new_pos) &&
        snake_is_adjacent_position_free(snake, &new_pos) &&
        led_cube_is_node_off(snake->led_cube, new_pos.x, new_pos.y, new_pos.z))
    {
      memcpy(&snake->speed, &new_speed, sizeof(snake_speed_t));
      snake_node_t new_head;
      new_head.p.x = new_pos.x;
      new_head.p.y = new_pos.y;
      new_head.p.z = new_pos.z;
      new_head.b = snake->nodes[0].b;
      snake_move_to_node(snake, &new_head);
      return;
    }
    idx++;
    if (idx >= SNAKE_ROTATION_MATRIX_SIZE)
    {
      idx = 0;
    }
  }

  snake_set_default(snake);
}

/** @} */
