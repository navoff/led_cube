/**
 * @file led_cube.c
 * @brief
 * @author Vladimir Postnov
 * @date 07/06/2020
 * @addtogroup
 * @{
 */

#include "config.h"

#include <string.h>

#include "drivers/gpio.h"
#include "drivers/tim.h"
#include "drivers/tick_timer.h"
#include "utils/check_params.h"
#include "led.h"

#include "led_cube.h"

// ======================================= Definition ==============================================

typedef struct
{
  GPIO_TypeDef *gpio;
  uint16_t pin;
  uint8_t gpio_idx;
  uint8_t pin_idx;
} led_cube_cell_conf_t;

#if (HW_TYPE == HW_TYPE__LED_CUBE_NUCLEO_F103RB)
const led_cube_cell_conf_t led_cube_x_plane[LED_CUBE_X_SIZE] =
{
    { GPIOB, GPIO_Pin_4,  1, 4  },
    { GPIOC, GPIO_Pin_9,  2, 9  },
    { GPIOB, GPIO_Pin_10, 1, 10 },
    { GPIOB, GPIO_Pin_8,  1, 8  },
};

const led_cube_cell_conf_t led_cube_yz_line[LED_CUBE_Y_SIZE][LED_CUBE_Z_SIZE] =
{
    {
        { GPIOB, GPIO_Pin_3,  1, 3  },
        { GPIOC, GPIO_Pin_4,  2, 4  },
        { GPIOA, GPIO_Pin_10, 0, 10 },
        { GPIOB, GPIO_Pin_5,  1, 5  },
    },
    {
        { GPIOB, GPIO_Pin_14, 1, 14 },
        { GPIOB, GPIO_Pin_15, 1, 15 },
        { GPIOB, GPIO_Pin_1,  1, 1  },
        { GPIOB, GPIO_Pin_13, 1, 13 },
    },
    {
        { GPIOB, GPIO_Pin_12, 1, 12 },
        { GPIOA, GPIO_Pin_11, 0, 11 },
        { GPIOB, GPIO_Pin_11, 1, 11 },
        { GPIOB, GPIO_Pin_2,  1, 2  },
    },
    {
        { GPIOC, GPIO_Pin_6,  2, 6  },
        { GPIOC, GPIO_Pin_8,  2, 8  },
        { GPIOC, GPIO_Pin_5,  2, 5  },
        { GPIOA, GPIO_Pin_12, 0, 12 },
    },
};

#define LED_CUBE_YZ_LINE_ON(y, z)     led_cube_yz_line[y][z].gpio->BRR = led_cube_yz_line[y][z].pin
#define LED_CUBE_YZ_LINE_OFF(y, z)    led_cube_yz_line[y][z].gpio->BSRR = led_cube_yz_line[y][z].pin

#elif (HW_TYPE == HW_TYPE__LED_CUBE_4X4X4_REV001)
const led_cube_cell_conf_t led_cube_x_plane[LED_CUBE_X_SIZE] =
{
    { GPIOD, GPIO_Pin_9,  3, 9  },
    { GPIOD, GPIO_Pin_15, 3, 15 },
    { GPIOD, GPIO_Pin_5,  3, 5  },
    { GPIOB, GPIO_Pin_6,  1, 6  },
};

const led_cube_cell_conf_t led_cube_yz_line[LED_CUBE_Y_SIZE][LED_CUBE_Z_SIZE] =
{
    {
        { GPIOE, GPIO_Pin_10, 4, 10 },
        { GPIOE, GPIO_Pin_11, 4, 11 },
        { GPIOE, GPIO_Pin_12, 4, 12 },
        { GPIOE, GPIO_Pin_13, 4, 13 },
    },
    {
        { GPIOE, GPIO_Pin_14, 4, 14 },
        { GPIOE, GPIO_Pin_15, 4, 15 },
        { GPIOB, GPIO_Pin_10, 1, 10 },
        { GPIOB, GPIO_Pin_11, 1, 11 },
    },
    {
        { GPIOD, GPIO_Pin_8 , 3, 8  },
        { GPIOB, GPIO_Pin_15, 1, 15 },
        { GPIOB, GPIO_Pin_14, 1, 14 },
        { GPIOB, GPIO_Pin_13, 1, 13 },
    },
    {
        { GPIOD, GPIO_Pin_13, 3, 13 },
        { GPIOD, GPIO_Pin_12, 3, 12 },
        { GPIOD, GPIO_Pin_11, 3, 11 },
        { GPIOD, GPIO_Pin_10, 3, 10 },
    },
};

#define LED_CUBE_YZ_LINE_ON(y, z)     led_cube_yz_line[y][z].gpio->BRR = led_cube_yz_line[y][z].pin
#define LED_CUBE_YZ_LINE_OFF(y, z)    led_cube_yz_line[y][z].gpio->BSRR = led_cube_yz_line[y][z].pin

#elif (HW_TYPE == HW_TYPE__LED_CUBE_8X8X8_REV001)
const led_cube_cell_conf_t led_cube_x_plane[LED_CUBE_X_SIZE] =
{
    { GPIOB, GPIO_Pin_13,  1, 13 },
    { GPIOG, GPIO_Pin_8,   6, 8  },
    { GPIOB, GPIO_Pin_12,  1, 12 },
    { GPIOF, GPIO_Pin_5,   5, 5  },
    { GPIOA, GPIO_Pin_12,  0, 12 },
    { GPIOE, GPIO_Pin_1,   4, 1  },
    { GPIOD, GPIO_Pin_1,   3, 1  },
    { GPIOB, GPIO_Pin_9,   1, 9  },
};

const led_cube_cell_conf_t led_cube_yz_line[LED_CUBE_Y_SIZE][LED_CUBE_Z_SIZE] =
{
    {
        { GPIOF, GPIO_Pin_6,  5, 6  },
        { GPIOF, GPIO_Pin_7,  5, 7  },
        { GPIOF, GPIO_Pin_8,  5, 8  },
        { GPIOF, GPIO_Pin_9,  5, 9  },
        { GPIOF, GPIO_Pin_10, 5, 10 },
        { GPIOC, GPIO_Pin_0,  2, 0  },
        { GPIOC, GPIO_Pin_1,  2, 1  },
        { GPIOC, GPIO_Pin_2,  2, 2  },
    },
    {
        { GPIOC, GPIO_Pin_3, 2, 3 },
        { GPIOA, GPIO_Pin_0, 0, 0 },
        { GPIOA, GPIO_Pin_1, 0, 1 },
        { GPIOA, GPIO_Pin_2, 0, 2 },
        { GPIOA, GPIO_Pin_3, 0, 3 },
        { GPIOA, GPIO_Pin_4, 0, 4 },
        { GPIOA, GPIO_Pin_5, 0, 5 },
        { GPIOA, GPIO_Pin_6, 0, 6 },
    },
    {
        { GPIOA, GPIO_Pin_7,  0, 7  },
        { GPIOC, GPIO_Pin_4,  2, 4  },
        { GPIOC, GPIO_Pin_5,  2, 5  },
        { GPIOB, GPIO_Pin_0,  1, 0  },
        { GPIOB, GPIO_Pin_1,  1, 1  },
        { GPIOB, GPIO_Pin_2,  1, 2  },
        { GPIOF, GPIO_Pin_11, 5, 11 },
        { GPIOF, GPIO_Pin_12, 5, 12 },
    },
    {
        { GPIOE, GPIO_Pin_9,  4, 9  },
        { GPIOE, GPIO_Pin_8,  4, 8  },
        { GPIOE, GPIO_Pin_7,  4, 7  },
        { GPIOG, GPIO_Pin_1,  6, 1  },
        { GPIOG, GPIO_Pin_0,  6, 0  },
        { GPIOF, GPIO_Pin_15, 5, 15 },
        { GPIOF, GPIO_Pin_14, 5, 14 },
        { GPIOF, GPIO_Pin_13, 5, 13 },
    },
    {
        { GPIOB, GPIO_Pin_11, 1, 11 },
        { GPIOB, GPIO_Pin_10, 1, 10 },
        { GPIOE, GPIO_Pin_15, 4, 15 },
        { GPIOE, GPIO_Pin_14, 4, 14 },
        { GPIOE, GPIO_Pin_13, 4, 13 },
        { GPIOE, GPIO_Pin_12, 4, 12 },
        { GPIOE, GPIO_Pin_11, 4, 11 },
        { GPIOE, GPIO_Pin_10, 4, 10 },
    },
    {
        { GPIOD, GPIO_Pin_13, 3, 13 },
        { GPIOD, GPIO_Pin_12, 3, 12 },
        { GPIOD, GPIO_Pin_11, 3, 11 },
        { GPIOD, GPIO_Pin_10, 3, 10 },
        { GPIOD, GPIO_Pin_9,  3, 9  },
        { GPIOD, GPIO_Pin_8,  3, 8  },
        { GPIOB, GPIO_Pin_15, 1, 15 },
        { GPIOB, GPIO_Pin_14, 1, 14 },
    },
    {
        { GPIOG, GPIO_Pin_7,  6, 7  },
        { GPIOG, GPIO_Pin_6,  6, 6  },
        { GPIOG, GPIO_Pin_5,  6, 5  },
        { GPIOG, GPIO_Pin_4,  6, 4  },
        { GPIOG, GPIO_Pin_3,  6, 3  },
        { GPIOG, GPIO_Pin_2,  6, 2  },
        { GPIOD, GPIO_Pin_15, 3, 15 },
        { GPIOD, GPIO_Pin_14, 3, 14 },
    },
    {
        { GPIOA, GPIO_Pin_11, 0, 11 },
        { GPIOA, GPIO_Pin_10, 0, 10 },
        { GPIOA, GPIO_Pin_9,  0, 9  },
        { GPIOA, GPIO_Pin_8,  0, 8  },
        { GPIOC, GPIO_Pin_9,  2, 9  },
        { GPIOC, GPIO_Pin_8,  2, 8  },
        { GPIOC, GPIO_Pin_7,  2, 7  },
        { GPIOC, GPIO_Pin_6,  2, 6  },
    },
};

#define LED_CUBE_YZ_LINE_ON(y, z)     led_cube_yz_line[y][z].gpio->BSRRH = led_cube_yz_line[y][z].pin
#define LED_CUBE_YZ_LINE_OFF(y, z)    led_cube_yz_line[y][z].gpio->BSRRL = led_cube_yz_line[y][z].pin

#else
  #error Unknown hw type
#endif

#define LED_CUBE_X_PLANE_ON(x)        gpio_pin_cmd(led_cube_x_plane[x].gpio_idx, \
                                                   led_cube_x_plane[x].pin_idx, \
                                                   ENABLE)
#define LED_CUBE_X_PLANE_OFF(x)       gpio_pin_cmd(led_cube_x_plane[x].gpio_idx, \
                                                   led_cube_x_plane[x].pin_idx, \
                                                   DISABLE)

void led_cube_gpio_init(void);
void led_cube_timer_init(void);
void led_cube_turn_on_current_plane(led_cube_t *led_cube);
void led_cube_turn_off_current_plane(led_cube_t *led_cube);
void led_cube_update_plane(led_cube_t *led_cube, uint8_t x);

// ======================================== Implementation =========================================

void led_cube_init(led_cube_t *led_cube)
{
  memset(led_cube, 0, sizeof(led_cube_t));

  led_cube_gpio_init();
  led_cube_timer_init();
}

void led_cube_start_timer(led_cube_t *led_cube)
{
  TIM_Cmd(tim_hw_items[LED_CUBE_TIM_NUMBER - 1].timer, ENABLE);
}

void led_cube_test(led_cube_t *led_cube)
{
  for (uint8_t x = 0; x < LED_CUBE_X_SIZE; ++x)
  {
    LED_CUBE_X_PLANE_ON(x);
    for (uint8_t y = 0; y < LED_CUBE_Y_SIZE; ++y)
    {
      for (uint8_t z = 0; z < LED_CUBE_Z_SIZE; ++z)
      {
        LED_CUBE_YZ_LINE_ON(y, z);
        tick_timer_ms_delay(100);
        LED_CUBE_YZ_LINE_OFF(y, z);
      }
    }
    LED_CUBE_X_PLANE_OFF(x);
  }
}

void led_cube_gpio_init(void)
{
  for (uint8_t i = 0; i < LED_CUBE_X_SIZE; ++i)
  {
    const led_cube_cell_conf_t *conf = &led_cube_x_plane[i];
    gpio_rcc_cmd(conf->gpio_idx, ENABLE);
    gpio_pin_set_mode(conf->gpio_idx, conf->pin_idx, GPIO_MODE_OUT_PP);
    LED_CUBE_X_PLANE_OFF(i);
  }

  for (uint8_t i = 0; i < LED_CUBE_Y_SIZE; ++i)
  {
    for (uint8_t k = 0; k < LED_CUBE_Z_SIZE; ++k)
    {
      const led_cube_cell_conf_t *conf = &led_cube_yz_line[i][k];
      gpio_rcc_cmd(conf->gpio_idx, ENABLE);
      gpio_pin_set_mode(conf->gpio_idx, conf->pin_idx, GPIO_MODE_OUT_PP);

      LED_CUBE_YZ_LINE_OFF(i, k);
    }
  }
}

void led_cube_timer_init(void)
{
  tim_rcc_cmd(LED_CUBE_TIM_NUMBER, ENABLE);
  const tim_hw_item_t *tim_hw = &tim_hw_items[LED_CUBE_TIM_NUMBER - 1];

  TIM_TimeBaseInitTypeDef init_struct;
  TIM_TimeBaseStructInit(&init_struct);
  init_struct.TIM_CounterMode = TIM_CounterMode_Up;
  init_struct.TIM_ClockDivision = TIM_CKD_DIV1;
  init_struct.TIM_Prescaler = LED_CUBE_TIMER_PRESCALER;
  init_struct.TIM_Period = LED_CUBE_TIMER_PERIOD - 1;
  init_struct.TIM_RepetitionCounter = 0;
  TIM_TimeBaseInit(tim_hw->timer, &init_struct);

  TIM_ClearITPendingBit(tim_hw->timer, TIM_IT_Update);
  TIM_ITConfig(tim_hw->timer, TIM_IT_Update, ENABLE);

  NVIC_InitTypeDef nvic_init_struct;
  nvic_init_struct.NVIC_IRQChannel = LED_CUBE_TIM_IRQ;
  nvic_init_struct.NVIC_IRQChannelPreemptionPriority = 0x0F;
  nvic_init_struct.NVIC_IRQChannelSubPriority = 0x0F;
  nvic_init_struct.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&nvic_init_struct);
}

void led_cube_process_cell(led_cube_cell_t *cell)
{
  if (cell->mode == LED_CUBE_CELL_ON_MODE__OFF)
  {
  }
  else if (cell->mode == LED_CUBE_CELL_ON_MODE__AUTO_OFF)
  {
    if (cell->state == LED_CUBE_CELL_STATE__ON_IN_PROG)
    {
      cell->counter++;
      if (cell->counter >= 3) // todo вынести в настройки
      {
        cell->counter = 0;
        cell->current += 1;
        if (cell->current >= cell->target)
        {
          cell->current = cell->target;
          cell->state = LED_CUBE_CELL_STATE__OFF_IN_PROG;
        }
      }
    }
    else if (cell->state == LED_CUBE_CELL_STATE__OFF_IN_PROG)
    {
      cell->counter++;
      if (cell->counter >= 3) // todo вынести в настройки
      {
        cell->counter = 0;
        cell->current -= 1;
        if (cell->current == 0)
        {
          cell->state = LED_CUBE_CELL_STATE__OFF;
        }
      }
    }
  }
  else if (cell->mode == LED_CUBE_CELL_ON_MODE__ON)
  {

  }
  else
  {
    check_params(false);
  }
}

void led_cube_irq_handler(led_cube_t *led_cube)
{
  LED_ON();
  bool need_update = false;
  led_cube_turn_off_current_plane(led_cube);
  led_cube->x_plane_counter++;
  if (led_cube->x_plane_counter >= LED_CUBE_X_SIZE)
  {
    led_cube->x_plane_counter = 0;

    led_cube->brightness_counter++;
    if (led_cube->brightness_counter >= LED_CUBE_MAX_BRIGHTNESS)
    {
      led_cube->brightness_counter = 0;
      need_update = true;
    }
  }
  led_cube_update_plane(led_cube, led_cube->x_plane_counter);
  led_cube_turn_on_current_plane(led_cube);
  LED_OFF();

  if (need_update)
  {
    for (uint8_t x = 0; x < LED_CUBE_X_SIZE; ++x)
    {
      for (uint8_t y = 0; y < LED_CUBE_Y_SIZE; ++y)
      {
        for (uint8_t z = 0; z < LED_CUBE_Z_SIZE; ++z)
        {
          led_cube_cell_t *cell = &led_cube->cells[x][y][z];
          led_cube_process_cell(cell);
        }
      }
    }
  }
}

void led_cube_set_cell(
    led_cube_t *led_cube,
    uint8_t x, uint8_t y, uint8_t z, uint8_t brightness,
    led_cube_cell_on_mode_t mode)
{
  check_params((x < LED_CUBE_X_SIZE) && (y < LED_CUBE_Y_SIZE) && (z < LED_CUBE_Z_SIZE));

  uint32_t save;
  ENTER_CRITICAL(save);

  led_cube_cell_t *cell = &led_cube->cells[x][y][z];
  if (mode == LED_CUBE_CELL_ON_MODE__AUTO_OFF)
  {
    cell->mode = mode;
    cell->state = LED_CUBE_CELL_STATE__ON_IN_PROG;
//    cell->current = 0; xxx
    cell->target = brightness;
    cell->counter = 0;
  }
  else if (mode == LED_CUBE_CELL_ON_MODE__ON)
  {
    cell->mode = mode;
    cell->state = LED_CUBE_CELL_STATE__ON_IN_PROG;
    cell->current = brightness;
  }
  else
  {
    check_params(false);
  }

  LEAVE_CRITICAL(save);
}

void led_cube_set_plane(led_cube_t *led_cube, uint8_t x, uint8_t b, led_cube_cell_on_mode_t mode)
{
  for (uint8_t y = 0; y < LED_CUBE_Y_SIZE; ++y)
  {
    for (uint8_t z = 0; z < LED_CUBE_Z_SIZE; ++z)
    {
      led_cube_set_cell(led_cube, x, y, z, b, mode);
    }
  }
}

bool led_cube_is_node_off(led_cube_t *led_cube, uint8_t x, uint8_t y, uint8_t z)
{
  bool res = false;
  led_cube_cell_t *cell = &led_cube->cells[x][y][z];

  uint32_t save;
  ENTER_CRITICAL(save);
  if (cell->state == LED_CUBE_CELL_STATE__OFF)
  {
    res = true;
  }
  LEAVE_CRITICAL(save);

  return res;
}

void led_cube_turn_on_current_plane(led_cube_t *led_cube)
{
  LED_CUBE_X_PLANE_ON(led_cube->x_plane_counter);
}


void led_cube_turn_off_current_plane(led_cube_t *led_cube)
{
  LED_CUBE_X_PLANE_OFF(led_cube->x_plane_counter);
}

void led_cube_update_plane(led_cube_t *led_cube, uint8_t x)
{
  for (uint8_t y = 0; y < LED_CUBE_Y_SIZE; ++y)
  {
    for (uint8_t z = 0; z < LED_CUBE_Z_SIZE; ++z)
    {
      if (led_cube->cells[x][y][z].current > led_cube->brightness_counter)
      {
        LED_CUBE_YZ_LINE_ON(y, z);
      }
      else
      {
        LED_CUBE_YZ_LINE_OFF(y, z);
      }
    }
  }
}

/** @} */
