/**
 * @file btns.h
 * @brief
 * @author Постнов В.М.
 * @date 11 мар. 2017 г.
 * @addtogroup
 * @{
*/

#ifndef BTNS_H_
#define BTNS_H_

// ======================================= Definition ==============================================

typedef struct
{
  uint8_t btn_idx;
  enum
  {
    BTN_EVENT_T__NONE,
    BTN_EVENT_T__CLICK,
    BTN_EVENT_T__LONGPRESS,
  } type;
} btn_event_t;

// ======================================= Declaration =============================================

void btns_init(void);
void btns_process(void);
void btns_get_and_clear_last_event(btn_event_t *last_event);

#endif /* BTNS_H_ */

/** @} */
