/**
 * @file snake.h
 * @brief
 * @author Vladimir Postnov
 * @date 10/06/2020
 * @addtogroup
 * @{
 */

#ifndef SNAKE_H_
#define SNAKE_H_

#include "led_cube.h"

// ========================================= Definition ============================================

#define SNAKE_MAK_LENGTH                  10
#define SNAKE_SPACE_DIMENSION             3

typedef union
{
  struct
  {
    int8_t x;
    int8_t y;
    int8_t z;
  };
  int8_t p[SNAKE_SPACE_DIMENSION];
} snake_position_t;

typedef union
{
  struct
  {
    int8_t x;
    int8_t y;
    int8_t z;
  };
  int8_t s[SNAKE_SPACE_DIMENSION];
} snake_speed_t;

typedef struct
{
  snake_position_t p;
  uint8_t b;
} snake_node_t;

typedef int8_t snake_rotation_matrix_t[SNAKE_SPACE_DIMENSION][SNAKE_SPACE_DIMENSION];

// ========================================= Declaration ===========================================

typedef struct
{
  led_cube_t *led_cube;
  snake_node_t nodes[SNAKE_MAK_LENGTH];
  uint16_t length;
  snake_speed_t speed;
} snake_t;

void snake_init(snake_t *snake, led_cube_t *led_cube);
void snake_next_step(snake_t *snake);
void snake_set_default(snake_t *snake);

#endif /* SNAKE_H_ */

/** @} */
