/**
 * @file led.h
 * @brief
 * @author Vladimir Postnov
 * @date Jul 21, 2020
 * @addtogroup
 * @{
*/

#ifndef LED_H_
#define LED_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "config.h"
#include "drivers/gpio.h"

// ========================================= Definition ============================================

#define LED_TOOGLE()            gpio_pin_toogle(LED_GPIO_IDX, LED_PIN_IDX)
#define LED_ON()                gpio_pin_cmd(LED_GPIO_IDX, LED_PIN_IDX, ENABLE)
#define LED_OFF()               gpio_pin_cmd(LED_GPIO_IDX, LED_PIN_IDX, DISABLE)

// ========================================= Declaration ===========================================

void led_init(void);

#ifdef __cplusplus
}
#endif

#endif /* LED_H_ */

/** @} */
