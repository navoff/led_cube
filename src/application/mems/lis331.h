/**
 * @file lis331.h
 * @author Постнов В.М.
 * @date 8 апр. 2018 г.
 * @brief
 * @addtogroup
 * @{
*/

#ifndef LIS331_H_
#define LIS331_H_

#include "mems/mems_types.h"

// ======================================= Definition ==============================================

// ======================================= Declaration =============================================

void lis331_init(mems_rx_data_handler_t rx_data_handler);
void lis331_drdy_handler(void);
void lis331_get_status(mems_status_t *status);

#endif /* LIS331_H_ */

/** @} */
