/**
 * @file mems.c
 * @author Постнов В.М.
 * @date 28 июл. 2019 г.
 * @brief
 * @addtogroup
 * @{
*/

#include "config.h"

#include <math.h>
#include <stdbool.h>
#include <string.h>

#include "drivers/tick_timer.h"
#if (FEATURE_ENABLE_LIS331)
  #include "mems/lis331.h"
#endif
#if (FEATURE_ENABLE_LSM6DS3)
  #include "mems/lsm6ds3.h"
#endif
#include "mems/mems_types.h"
#include "utils/lp_filter.h"

#include "mems/mems.h"

// ========================================= Definition ============================================

/// Параметр b ФНЧ наклона (описание тут lp_filter.h)
#define MEMS_TILT_LPF_B                       5000
/// Параметр k ФНЧ наклона (описание тут lp_filter.h)
#define MEMS_TILT_LPF_K                       16
/// Время выхода на режим фильтра наклона, мс
#define MEMS_TILT_INIT_FILTER_TIME            2000

/// Параметр b ФНЧ удара (описание тут lp_filter.h)
#define MEMS_HIT_LPF_B                        5000
/// Параметр k ФНЧ удара (описание тут lp_filter.h)
#define MEMS_HIT_LPF_K                        16
/// Размер буфера для вычесления СКО
#define MEMS_HIT_DEVIATION_BUF_SIZE           20
/// Время выхода на режим фильтров удара, мс
#define MEMS_HIT_INIT_FILTER_TIME             500
/// Если уровень удара опустился ниже этого уровня, то считается, что удар закончился.
/// Тут задаётся обратная величина порога
#define MEMS_HIT_THRESHOLD_HYST               2

#define MEMS_HIT_THRESHOLD                    2 // xxx

#if !defined(M_PI)
  #define M_PI                                3.14159265
#endif

/// Структура для обработки наклона
typedef struct
{
  /// Выполнена ли инициализация блока
  bool is_inited;

  /// Вышел ли ФНЧ на режим
  bool is_lpf_valid;

  /// Временная метка инициализации блока, мс
  uint32_t init_timestamp;

  /// ФНЧ по осям
  lp_filter_float_t axis_lpf[MEMS_AXES_AMOUNT];
} mems_tilt_t;

/// Буфер для рассчёта СКО
typedef struct
{
  /// Буфер со значениями квадратов ускорения
  float buf[MEMS_HIT_DEVIATION_BUF_SIZE];

  /// Текущее значение сумм квадратов ускорений
  float sum;

  //// Индекс в буфере, куда будет помещено следующее значение квадрата ускорения
  uint16_t idx;
} mems_hit_deviation_t;

/// Структура для обработки удара
typedef struct
{
  /// Выполнена ли инициализация блока
  bool is_inited;

  /// Вышел ли ФНЧ на режим
  bool is_lpf_valid;

  /// Был зафиксирован удра, ожидание окончания удара
  bool is_hit_detected;

  /// Временная метка инициализации блока, мс
  uint32_t init_timestamp;

  /// ФНЧ по осям
  lp_filter_float_t axis_lpf[MEMS_AXES_AMOUNT];

  /// Буферы по осям для рассчёта СКО
  mems_hit_deviation_t axis_deviation[MEMS_AXES_AMOUNT];

  /// Оценка уровня удара, mg
  float out;

  /// Текущее состояние события удара
  mems_hit_event_t event;
} mems_hit_t;

typedef struct
{
  mems_tilt_t tilt;
  mems_hit_t hit;
} mems_t;

// ========================================= Declaration ===========================================

void mems_tilt_rx_data_process(mems_tilt_t *tilt, mems_acc_t *acc);
void mems_hit_rx_data_process(mems_hit_t *hit, mems_acc_t *acc);
void mems_rx_data_handler(uint32_t timestamp, mems_acc_t *acc);

mems_t mems;

// ======================================== Implementation =========================================

void mems_init(void)
{
  mems.tilt.is_inited = false;
  mems.tilt.is_lpf_valid = false;
  
  mems.hit.is_inited = false;
  mems.hit.is_lpf_valid = false;
  mems.hit.is_hit_detected = false;
  mems.hit.out = 0;
  mems.hit.event.count = 0;
  mems.hit.event.max_level = 0;

  #if (FEATURE_ENABLE_LIS331)
  lis331_init(mems_rx_data_handler);
  #endif

  #if (FEATURE_ENABLE_LSM6DS3)
  lsm6ds3_init(mems_rx_data_handler);
  #endif
}

void mems_get_angles(mems_angles_t *angles)
{
  if ((mems.tilt.is_inited == false) || (mems.tilt.is_lpf_valid == false))
  {
    return;
  }

  angles->x = mems.tilt.axis_lpf[1].out;
  angles->y = -mems.tilt.axis_lpf[0].out;
  angles->z = mems.tilt.axis_lpf[2].out;

  angles->zx_plane = atan2f(angles->x, angles->z) * 180 / M_PI;
  angles->zy_plane = atan2f(angles->y, angles->z) * 180 / M_PI;
}

/**
 * @brief Прочитать и сбросить последнее событие удара
 * @param event - возвращаемое событие удара
 */
void mems_get_and_clear_hit_event(mems_hit_event_t *event)
{
  if (event != NULL)
  {
    memcpy(event, &mems.hit.event, sizeof(mems_hit_event_t));
  }

  mems.hit.event.count = 0;
  mems.hit.event.max_level = 0;
}

void mems_get_status(mems_status_t *status)
{
  #if (FEATURE_ENABLE_LSM6DS3)
  lsm6ds3_get_status(status);
  #elif (FEATURE_ENABLE_LIS331)
  lis331_get_status(status);
  #else
  status->is_active = 0;
  status->reserved = 0;
  #endif
}

/**
 * @brief Обработать принятые значения ускорений
 * @param timestamp - временная метка, когда приняты ускорения
 * @param acc - значения ускорений
 */
void mems_rx_data_handler(uint32_t timestamp, mems_acc_t *acc)
{
  mems_tilt_rx_data_process(&mems.tilt, acc);
  mems_hit_rx_data_process(&mems.hit, acc);

  #if (FEATURE_ENABLE_ARB_MEMS_EVENT_BY_USB)
  arb_driver_tx_mems_event_by_usb(timestamp, acc, mems.hit.out);
  #endif
}

/**
 * @brief Обработать принятые значения ускорений автоматом наклона
 * @param tilt - указатель на автомат наклона
 * @param acc - значения ускорений
 */
void mems_tilt_rx_data_process(mems_tilt_t *tilt, mems_acc_t *acc)
{
  // если фильтры еще не инициализированы
  if (tilt->is_inited == false)
  {
    tilt->is_inited = true;
    for (int i = 0; i < MEMS_AXES_AMOUNT; i++)
    {
      lp_filter_float_reset(
          &tilt->axis_lpf[i], acc->buf[i], MEMS_TILT_LPF_B, MEMS_TILT_LPF_K);
    }
    START_TIMER(tilt->init_timestamp);
  }
  // если фильтры еще готовы выдать корректные данные
  else if (tilt->is_lpf_valid == false)
  {
    if (CHECK_TIMER(tilt->init_timestamp, MEMS_TILT_INIT_FILTER_TIME))
    {
      tilt->is_lpf_valid = true;
    }
  }

  for (int i = 0; i < MEMS_AXES_AMOUNT; i++)
  {
    lp_filter_float_process(&tilt->axis_lpf[i], acc->buf[i]);
  }
}

/**
 * @brief Обработать принятые значения ускорений автоматом удара
 * @param hit - указатель на автомат удара
 * @param acc - значения ускорений
 */
void mems_hit_rx_data_process(mems_hit_t *hit, mems_acc_t *acc)
{
  // если фильтры еще не инициализированы
  if (hit->is_inited == false)
  {
    hit->is_inited = true;

    for (int i = 0; i < MEMS_AXES_AMOUNT; i++)
    {
      lp_filter_float_reset(
          &hit->axis_lpf[i], acc->buf[i], MEMS_HIT_LPF_B, MEMS_HIT_LPF_K);

      memset(&hit->axis_deviation[i], 0, sizeof(mems_hit_deviation_t));
    }
  }
  // если фильтры еще готовы выдать корректные данные
  else if (hit->is_lpf_valid == false)
  {
    if (CHECK_TIMER(hit->init_timestamp, MEMS_HIT_INIT_FILTER_TIME))
    {
      hit->is_lpf_valid = true;
    }
  }

  for (int i = 0; i < MEMS_AXES_AMOUNT; i++)
  {
    lp_filter_float_process(&hit->axis_lpf[i], acc->buf[i]);

    mems_hit_deviation_t *deviation = &hit->axis_deviation[i];

    deviation->sum -= deviation->buf[deviation->idx];
    deviation->buf[deviation->idx] =
        (acc->buf[i] - hit->axis_lpf[i].out) * (acc->buf[i] - hit->axis_lpf[i].out);
    deviation->sum += deviation->buf[deviation->idx];

    deviation->idx++;
    if (deviation->idx >= MEMS_HIT_DEVIATION_BUF_SIZE)
    {
      deviation->idx = 0;
    }
  }

  if (hit->is_lpf_valid == false)
  {
    return;
  }

  float sum = 0;
  for (int i = 0; i < MEMS_AXES_AMOUNT; i++)
  {
    sum += hit->axis_deviation[i].sum;
  }
  hit->out = sqrtf(sum);

  uint16_t hit_level = (uint16_t)roundf(hit->out * 0.064); // пересчёт: mg -> 1/64g
  if (hit->is_hit_detected)
  {
    if (hit_level > hit->event.max_level)
    {
      hit->event.max_level = hit_level;
    }

    if (hit_level * MEMS_HIT_THRESHOLD_HYST < MEMS_HIT_THRESHOLD)
    {
      hit->is_hit_detected = false;
    }
  }
  else
  {
    float threshold_hit_up = MEMS_HIT_THRESHOLD;
    if (hit_level >= threshold_hit_up)
    {
      hit->is_hit_detected = true;
      if (hit_level > hit->event.max_level)
      {
        hit->event.max_level = hit_level;
      }
      hit->event.count++;
    }
  }
}

/** @} */
