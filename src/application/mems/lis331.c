/**
 * @file lis331.c
 * @author Vladimir Postnov
 * @date 8 апр. 2018 г.
 * @brief
 * @addtogroup
 * @{
*/

#include "config.h"

#if (FEATURE_ENABLE_LIS331)

#include <string.h>

#include "drivers/gpio.h"
#include "drivers/spi.h"
#include "drivers/tick_timer.h"

#include "mems/lis331.h"

// ======================================= Definition ==============================================

#define LIS331_ACC_RANGE__6G                      0
#define LIS331_ACC_RANGE__12G                     1
#define LIS331_ACC_RANGE__24G                     2

/// Диапазон ускорений
#define LIS331_ACC_RANGE                          LIS331_ACC_RANGE__6G

/**
 * @brief Настройки SPI для работы с датчиком
 * @addtogroup LIS331_SPI_SETTINGS
 * @{
 */
  #define LIS331_SPI_DATASIZE                     SPI_DATA_SIZE__8
  #define LIS331_SPI_CLOCK_POLARITY               SPI_POLARITY__HIGH
  #define LIS331_SPI_CLOCK_PHASE                  SPI_PHASE__2EDGE
  #define LIS331_SPI_FIRST_BIT                    SPI_FIRST_BIT__MSB
/** @} */

/**
 * @brief
 * @defgroup LIS331_REGISTERS
 * @{
 */
  #define LIS331_REG__WHO_AM_I                    0x0F
    #define LIS331_WHO_AM_I                       0x32

  #define LIS331_REG__CTRL_REG1                   0x20
    #define LIS331_CTRL_REG1__X_EN                0x01
    #define LIS331_CTRL_REG1__X_DIS               0x00

    #define LIS331_CTRL_REG1__Y_EN                0x02
    #define LIS331_CTRL_REG1__Y_DIS               0x00

    #define LIS331_CTRL_REG1__Z_EN                0x04
    #define LIS331_CTRL_REG1__Z_DIS               0x00

    #define LIS331_CTRL_REG1__DR_50HZ             0x00
    #define LIS331_CTRL_REG1__DR_100HZ            0x08
    #define LIS331_CTRL_REG1__DR_400HZ            0x10
    #define LIS331_CTRL_REG1__DR_1000HZ           0x18

    #define LIS331_CTRL_REG1__PM_POWER_DOWN       0x00
    #define LIS331_CTRL_REG1__PM_NORMAL_MODE      0x20
    #define LIS331_CTRL_REG1__PM_LOW_POWER_0HZ5   0x40
    #define LIS331_CTRL_REG1__PM_LOW_POWER_1HZ    0x60
    #define LIS331_CTRL_REG1__PM_LOW_POWER_2HZ    0x80
    #define LIS331_CTRL_REG1__PM_LOW_POWER_5HZ    0xA0
    #define LIS331_CTRL_REG1__PM_LOW_POWER_10HZ   0xC0

  #define LIS331_REG__CTRL_REG2                   0x21

  #define LIS331_REG__CTRL_REG3                   0x22
    // Data signal on INT 1 pad control bits
    #define LIS331_CTRL_REG3__I1_CFG_INT1         0x00
    #define LIS331_CTRL_REG3__I1_CFG_INT1_OR_INT2 0x01
    #define LIS331_CTRL_REG3__I1_CFG_DATA_READY   0x02
    #define LIS331_CTRL_REG3__I1_CFG_BOOT         0x04

    // Latch interrupt request on INT1_SRC register,
    // with INT1_SRC register cleared by reading INT1_SRC itself.
    #define LIS331_CTRL_REG3__LIR1_NOT_LATCHED    0x00
    #define LIS331_CTRL_REG3__LIR1_LATCHED        0x04

    // Data signal on INT 2 pad control bits
    #define LIS331_CTRL_REG3__I2_CFG_INT2         0x00
    #define LIS331_CTRL_REG3__I2_CFG_INT1_OR_INT2 0x08
    #define LIS331_CTRL_REG3__I2_CFG_DATA_READY   0x10
    #define LIS331_CTRL_REG3__I2_CFG_BOOT         0x18

    // Latch interrupt request on INT2_SRC register,
    // with INT2_SRC register cleared by reading INT2_SRC itself.
    #define LIS331_CTRL_REG3__LIR2_NOT_LATCHED    0x00
    #define LIS331_CTRL_REG3__LIR2_LATCHED        0x20

    // Push-pull/Open drain selection on interrupt pad
    #define LIS331_CTRL_REG3__PP_OD_PP            0x00
    #define LIS331_CTRL_REG3__PP_OD_OD            0x40

    // Interrupt active high, low
    #define LIS331_CTRL_REG3__IHL_HIGH            0x00
    #define LIS331_CTRL_REG3__IHL_LOW             0x80

  #define LIS331_REG__CTRL_REG4                   0x23
    // SPI serial interface mode selection
    #define LIS331_CTRL_REG4__SIM_4WIRE           0x00
    #define LIS331_CTRL_REG4__SIM_3WIRE           0x01

    // Self-test enable
    #define LIS331_CTRL_REG4__ST_OFF              0x00
    #define LIS331_CTRL_REG4__ST_ON               0x02
    #define LIS331_CTRL_REG4__ST_MSK              0x02

    // Self-test sign
    #define LIS331_CTRL_REG4__ST_SIGN_PLUS        0x00
    #define LIS331_CTRL_REG4__ST_SIGN_MINUS       0x08
    #define LIS331_CTRL_REG4__ST_SIGN_MSK         0x08

    // Full-scale selection
    #define LIS331_CTRL_REG4__FS_6G               0x00
    #define LIS331_CTRL_REG4__FS_12G              0x10
    #define LIS331_CTRL_REG4__FS_24G              0x30

    // Big/little endian data selection
    #define LIS331_CTRL_REG4__BLE_LSB             0x00
    #define LIS331_CTRL_REG4__BLE_MSB             0x40

    // Block data update
    #define LIS331_CTRL_REG4__BDU_CONTINUOS       0x00
    #define LIS331_CTRL_REG4__BDU_BETWEEN         0x80

  #define LIS331_REG__CTRL_REG5                   0x24
  #define LIS331_REG__STATUS_REG                  0x27

  #define LIS331_REG__OUT_X_L                     0x28
  #define LIS331_REG__OUT_X_H                     0x29
  #define LIS331_REG__OUT_Y_L                     0x2A
  #define LIS331_REG__OUT_Y_H                     0x2B
  #define LIS331_REG__OUT_Z_L                     0x2C
  #define LIS331_REG__OUT_Z_H                     0x2D
/** @} */

/**
 * @defgroup LIS331_SELF_TEST
 * Диапазон (максимальные и минимальные) "сырых" данных от акселерометра
 * при проведении самотестирования.
 * Значения взяты из документации на измеритель и указаны при диапазоне 6g.
 * @{
 */
  #define LIS331_SELF_TEST_RAW_VALUE_X_MIN        (50 << 4)
  #define LIS331_SELF_TEST_RAW_VALUE_X_MAX        (180 << 4)

  #define LIS331_SELF_TEST_RAW_VALUE_Y_MIN        (-180 << 4)
  #define LIS331_SELF_TEST_RAW_VALUE_Y_MAX        (-50 << 4)

  #define LIS331_SELF_TEST_RAW_VALUE_Z_MIN        (220 << 4)
  #define LIS331_SELF_TEST_RAW_VALUE_Z_MAX        (370 << 4)
/** @} */

#if (LIS331_ACC_RANGE == LIS331_ACC_RANGE__6G)
  #define LIS331_CTRL_REG4__FS                    LIS331_CTRL_REG4__FS_6G
  #define LIS331_CONVERT_RAW_TO_ACC(x)            ((((int32_t)x) >> 4) * 12000 / 4096)
#elif (LIS331_ACC_RANGE == LIS331_ACC_RANGE__12G)
  #define LIS331_CTRL_REG4__FS                    LIS331_CTRL_REG4__FS_12G
  #define LIS331_CONVERT_RAW_TO_ACC(x)            ((((int32_t)x) >> 4) * 24000 / 4096)
#elif (LIS331_ACC_RANGE == LIS331_ACC_RANGE__24G)
  #define LIS331_CTRL_REG4__FS                    LIS331_CTRL_REG4__FS_24G
  #define LIS331_CONVERT_RAW_TO_ACC(x)            ((((int32_t)x) >> 4) * 48000 / 4096)
#else
  #error Выбран не верный диапазон акселерометра
#endif

#define LIS331_READ_REG_MASK                      0x80
#define LIS331_MULTI_REG_MASK                     0x40

#define LIS331_SELF_TEST_WAIT_TIMEOUT             500

typedef struct
{
  mems_status_t status;
  mems_acc_t acc;
  mems_rx_data_handler_t rx_data_handler;

  spi_core_t spi_core;
  spi_device_t spi_device;
} lis331_t;

// ======================================= Declaration =============================================

void lis331_spi_core_init(spi_core_t *lis331_spi_core);
void lis331_spi_device_init(spi_device_t *lis331_spi_device, spi_core_t *lis331_spi_core);
void lis331_drdy_exti_init(void);
void lis331_read_acc(spi_device_t *spi_device, mems_acc_t *acc);
void lis331_wait_and_read_acc(
    spi_device_t *spi_device, mems_acc_t *acc, uint16_t number, uint32_t timeout);
void lis331_self_test(lis331_t *lis331);
uint8_t lis331_read_reg(spi_device_t *spi_device, uint8_t reg);
void lis331_write_array_regs(spi_device_t *spi_device, uint8_t *array, uint16_t size);
void lis331_read_array_regs(
    spi_device_t *spi_device, uint8_t *buf, uint8_t start_reg, uint16_t size);
void lis331_delay_after_write(void);

lis331_t lis331;

/// Диапазон (максимальные и минимальные) значений акселерометра при проведении самотестирования
/// @note Откуда эта 2-ка не ясно. В документации этого нет.
/// Пробовал только на 1-ом измерители и
/// у него по всем осям значение в 2-раз больше среднего из документации.
const mems_acc_t lis331_acc_self_test_range[2] =
{
    // минимальные значения
    {
        .x = LIS331_CONVERT_RAW_TO_ACC(LIS331_SELF_TEST_RAW_VALUE_X_MIN * 2),
        .y = LIS331_CONVERT_RAW_TO_ACC(LIS331_SELF_TEST_RAW_VALUE_Y_MIN * 2),
        .z = LIS331_CONVERT_RAW_TO_ACC(LIS331_SELF_TEST_RAW_VALUE_Z_MIN * 2),
    },
    // максимальные значения
    {
        .x = LIS331_CONVERT_RAW_TO_ACC(LIS331_SELF_TEST_RAW_VALUE_X_MAX * 2),
        .y = LIS331_CONVERT_RAW_TO_ACC(LIS331_SELF_TEST_RAW_VALUE_Y_MAX * 2),
        .z = LIS331_CONVERT_RAW_TO_ACC(LIS331_SELF_TEST_RAW_VALUE_Z_MAX * 2),
    }
};

const uint8_t lis331_cmd_start[] =
{
    LIS331_REG__CTRL_REG3, LIS331_CTRL_REG3__I1_CFG_DATA_READY |
                           LIS331_CTRL_REG3__LIR1_NOT_LATCHED |
                           LIS331_CTRL_REG3__I2_CFG_DATA_READY |
                           LIS331_CTRL_REG3__LIR2_NOT_LATCHED |
                           LIS331_CTRL_REG3__PP_OD_PP |
                           LIS331_CTRL_REG3__IHL_HIGH,
    LIS331_REG__CTRL_REG1, LIS331_CTRL_REG1__X_EN |
                           LIS331_CTRL_REG1__Y_EN |
                           LIS331_CTRL_REG1__Z_EN |
                           LIS331_CTRL_REG1__DR_50HZ |
                           LIS331_CTRL_REG1__PM_NORMAL_MODE,
};

const uint8_t lis331_cmd_stop[] =
{
    LIS331_REG__CTRL_REG1, LIS331_CTRL_REG1__X_DIS |
                           LIS331_CTRL_REG1__Y_DIS |
                           LIS331_CTRL_REG1__Z_DIS |
                           LIS331_CTRL_REG1__PM_POWER_DOWN,
};

const uint8_t lis331_cmd_self_test_en[] =
{
    LIS331_REG__CTRL_REG4, LIS331_CTRL_REG4__SIM_4WIRE |
                           LIS331_CTRL_REG4__ST_ON |
                           LIS331_CTRL_REG4__ST_SIGN_PLUS |
                           LIS331_CTRL_REG4__FS |
                           LIS331_CTRL_REG4__BLE_LSB |
                           LIS331_CTRL_REG4__BDU_CONTINUOS,
};

const uint8_t lis331_cmd_self_test_dis[] =
{
    LIS331_REG__CTRL_REG4, LIS331_CTRL_REG4__SIM_4WIRE |
                           LIS331_CTRL_REG4__ST_OFF |
                           LIS331_CTRL_REG4__FS |
                           LIS331_CTRL_REG4__BLE_LSB |
                           LIS331_CTRL_REG4__BDU_CONTINUOS,
};

// ====================================== Implementation ===========================================

void lis331_init(mems_rx_data_handler_t rx_data_handler)
{
  lis331_spi_core_init(&lis331.spi_core);
  lis331_spi_device_init(&lis331.spi_device, &lis331.spi_core);
  lis331.status.is_active = 0;
  lis331.status.reserved = 0;
  lis331.status.self_test_fail.msk = 0;
  lis331.rx_data_handler = rx_data_handler;

  // Проверка работоспособности
  uint8_t register_who_am_i = lis331_read_reg(&lis331.spi_device, LIS331_REG__WHO_AM_I);

  if (register_who_am_i == LIS331_WHO_AM_I)
  {
    lis331.status.is_active = 1;

    lis331_self_test(&lis331);

    lis331_drdy_exti_init();
    lis331_write_array_regs(
        &lis331.spi_device, (uint8_t *)lis331_cmd_start, sizeof(lis331_cmd_start));
  }
}

void lis331_drdy_handler(void)
{
  if (gpio_exti_check_status(LIS331_INT1_PIN_IDX))
  {
    gpio_exti_clear_status(LIS331_INT1_PIN_IDX);
    
    lis331_read_acc(&lis331.spi_device, &lis331.acc);

    if (lis331.rx_data_handler != NULL)
    {
      uint32_t timestamp;
      START_TIMER(timestamp);

      lis331.rx_data_handler(timestamp, &lis331.acc);
    }
  }
}

void lis331_get_status(mems_status_t *status)
{
  memcpy(status, &lis331.status, sizeof(mems_status_t));
}

void lis331_read_acc(spi_device_t *spi_device, mems_acc_t *acc)
{
  int16_t raw_data_buf[MEMS_AXES_AMOUNT];

  lis331_read_array_regs(
      spi_device,
      (uint8_t *)raw_data_buf,
      LIS331_REG__OUT_X_L,
      sizeof(raw_data_buf));

  for (uint8_t i = 0; i < MEMS_AXES_AMOUNT; i++)
  {
    acc->buf[i] = LIS331_CONVERT_RAW_TO_ACC(raw_data_buf[i]);
  }
}

void lis331_wait_and_read_acc(
    spi_device_t *spi_device, mems_acc_t *acc, uint16_t number, uint32_t timeout)
{
  uint32_t timeout_timestamp;
  START_TIMER(timeout_timestamp);

  for (uint16_t i = 0; i < number; i++)
  {
    while (gpio_pin_state(LIS331_INT1_GPIO_IDX, LIS331_INT1_PIN_IDX) == false)
    {
      if (CHECK_TIMER(timeout_timestamp, 500))
      {
        return;
      }
    }

    lis331_read_acc(spi_device, acc);
  }
}

void lis331_self_test(lis331_t *lis331)
{
  // включить датчик в режиме самотестирования
  lis331_write_array_regs(
      &lis331->spi_device, (uint8_t *)lis331_cmd_self_test_en, sizeof(lis331_cmd_self_test_en));
  lis331_write_array_regs(
      &lis331->spi_device, (uint8_t *)lis331_cmd_start, sizeof(lis331_cmd_start));

  // прочитать данные
  mems_acc_t acc_test_on;
  lis331_wait_and_read_acc(&lis331->spi_device, &acc_test_on, 2, LIS331_SELF_TEST_WAIT_TIMEOUT);

  // выключить датчик
  lis331_write_array_regs(
      &lis331->spi_device, (uint8_t *)lis331_cmd_stop, sizeof(lis331_cmd_stop));
  lis331_write_array_regs(
      &lis331->spi_device, (uint8_t *)lis331_cmd_self_test_dis, sizeof(lis331_cmd_self_test_dis));

  // включить датчик в обычном режиме
  lis331_write_array_regs(
      &lis331->spi_device, (uint8_t *)lis331_cmd_start, sizeof(lis331_cmd_start));

  // прочитать данные
  mems_acc_t acc_test_off;
  lis331_wait_and_read_acc(&lis331->spi_device, &acc_test_off, 2, LIS331_SELF_TEST_WAIT_TIMEOUT);

  // выключить датчик
  lis331_write_array_regs(
      &lis331->spi_device, (uint8_t *)lis331_cmd_stop, sizeof(lis331_cmd_stop));

  mems_acc_t acc_res;
  for (uint8_t i = 0; i < MEMS_AXES_AMOUNT; i++)
  {
    acc_res.buf[i] = acc_test_on.buf[i] - acc_test_off.buf[i];
  }

  lis331->status.self_test_fail.msk = 0;
  for (uint8_t i = 0; i < MEMS_AXES_AMOUNT; i++)
  {
    if (acc_res.buf[i] < lis331_acc_self_test_range[0].buf[i])
    {
      lis331->status.self_test_fail.min_msk |= (1 << i);
    }
    if (acc_res.buf[i] > lis331_acc_self_test_range[1].buf[i])
    {
      lis331->status.self_test_fail.max_msk |= (1 << i);
    }
  }
}

void lis331_spi_core_init(spi_core_t *lis331_spi_core)
{
  spi_core_init_struct_t spi_core_init_struct;

  spi_core_init_struct.tx_mode = SPI_LINE_MODE__POLLING;
  spi_core_init_struct.rx_mode = SPI_LINE_MODE__POLLING;
  spi_core_init_struct.spi_number = LIS331_SPI_NUMBER;

  spi_core_init_struct.clk_gpio_idx = LIS331_SPI_CLK_GPIO_IDX;
  spi_core_init_struct.clk_pin_idx = LIS331_SPI_CLK_PIN_IDX;

  spi_core_init_struct.miso_gpio_idx = LIS331_SPI_MISO_GPIO_IDX;
  spi_core_init_struct.miso_pin_idx = LIS331_SPI_MISO_PIN_IDX;

  spi_core_init_struct.mosi_gpio_idx = LIS331_SPI_MOSI_GPIO_IDX;
  spi_core_init_struct.mosi_pin_idx = LIS331_SPI_MOSI_PIN_IDX;

  spi_core_init(lis331_spi_core, &spi_core_init_struct);
}

void lis331_spi_device_init(spi_device_t *lis331_spi_device, spi_core_t *lis331_spi_core)
{
  lis331_spi_device->spi_core = lis331_spi_core;
  lis331_spi_device->cs_gpio_idx = LIS331_SPI_CS_GPIO_IDX;
  lis331_spi_device->cs_pin_idx = LIS331_SPI_CS_PIN_IDX;
  lis331_spi_device->settings.frequency = LIS331_SPI_FREQUENCY;
  lis331_spi_device->settings.data_size = LIS331_SPI_DATASIZE;
  lis331_spi_device->settings.first_bit = LIS331_SPI_FIRST_BIT;
  lis331_spi_device->settings.phase = LIS331_SPI_CLOCK_PHASE;
  lis331_spi_device->settings.polarity = LIS331_SPI_CLOCK_POLARITY;

  spi_device_init(lis331_spi_device);
}

void lis331_drdy_exti_init(void)
{
  gpio_rcc_cmd(LIS331_INT1_GPIO_IDX, ENABLE);
  gpio_exti_init(
      LIS331_INT1_GPIO_IDX,
      LIS331_INT1_PIN_IDX,
      GPIO_PIN_PULL__DOWN,
      GPIO_EXTI_EDGE__RISING,
      true);
}

/**
 * @brief Считать значение из регистра
 * @param spi_slave - экземпляр slave устройства SPI
 * @param reg - адрес регистра
 * @return Значение считанного регистра
 */
uint8_t lis331_read_reg(spi_device_t *spi_device, uint8_t reg)
{
  uint8_t reg_data = reg | LIS331_READ_REG_MASK;
  return spi_device_tx_rx_register(spi_device, reg_data, 0x00);
}

/**
 * @brief Функция отправки массива команд для записы в регистры датчика
 * @param spi_slave - экземпляр slave устройства SPI
 * @param array - массив из команда для записи в регистры. Должно быть четное число байт.
 *        Каждый 2 * i - адресс регистра, 2 * i + 1 - значение, записываемое по данному адресу
 * @param size - размер передаваемого массива
 */
void lis331_write_array_regs(spi_device_t *spi_device, uint8_t *array, uint16_t size)
{
  for (uint16_t i = 0; i < size / 2; i++)
  {
    spi_device_tx_rx_register(spi_device, array[2 * i], array[2 * i + 1]);
    lis331_delay_after_write();
  }
}

/**
 * @brief Считать значения нескольких регистров подряд
 * @param spi_device - экземпляр slave устройства SPI
 * @param buf - указатель на приемный буффер
 * @param start_reg - адрес регистра
 * @param size - колчество считываемых регистров
 */
void lis331_read_array_regs(
    spi_device_t *spi_device, uint8_t *buf, uint8_t start_reg, uint16_t size)
{
    uint8_t reg = start_reg | LIS331_READ_REG_MASK | LIS331_MULTI_REG_MASK;
    spi_device_tx_rx_array_registers(spi_device, reg, buf, size);
}

void lis331_delay_after_write(void)
{
  #if (HW_TYPE == HW_TYPE__LED_CUBE_8X8X8_REV001)
  // значения для создания паузы в 10 мкс
  for (uint32_t i = 0; i < 200; i++)
  {
      __NOP();
      __NOP();
      __NOP();
  }
  #else
    #error Не обработан тип платы
  #endif
}

#endif // #if (FEATURE_ENABLE_LIS331)

/** @} */
