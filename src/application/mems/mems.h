/**
 * @file mems.h
 * @author Постнов В.М.
 * @date 28 июл. 2019 г.
 * @brief
 * @addtogroup
 * @{
*/

#ifndef MEMS_H_
#define MEMS_H_

#include "mems/mems_types.h"

// ======================================= Definition ==============================================

typedef struct
{
  /// Количество зафиксированных ударов после последнего сброса события
  uint16_t count;

  /// Максимальной уровень зафиксированного удара после последнего сброса события, 1/64 g
  uint16_t max_level;
} mems_hit_event_t;

typedef struct
{
  float x;
  float y;
  float z;
  float zx_plane;
  float zy_plane;
} mems_angles_t;

// ======================================= Declaration =============================================

void mems_init(void);
void mems_get_angles(mems_angles_t *angles);
void mems_get_and_clear_hit_event(mems_hit_event_t *event);
void mems_get_status(mems_status_t *status);

#endif /* MEMS_H_ */

/** @} */
