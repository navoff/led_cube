/**
 * @file mic.c
 * @brief
 * @author Vladimir Postnov
 * @date Aug 31, 2020
 * @addtogroup
 * @{
*/

#include "config.h"

#if (FEATURE_ENABLE_MIC)

#include "drivers/dma.h"
#include "drivers/gpio.h"
#include "drivers/spi.h"

// ========================================= Definition ============================================

#define MIC_I2S_BUF_SIZE      512

// ========================================= Declaration ===========================================

extern void spi_rcc_cmd(uint8_t spi_number, FunctionalState new_state);

uint32_t mis_i2s_buf[MIC_I2S_BUF_SIZE];

// ======================================== Implementation =========================================

void mic_init(void)
{
  gpio_rcc_cmd(MIC_I2S_CLK_GPIO_IDX, ENABLE);
  gpio_rcc_cmd(MIC_I2S_SD_GPIO_IDX, ENABLE);
  gpio_rcc_cmd(MIC_I2S_WS_GPIO_IDX, ENABLE);

  gpio_pin_set_mode(MIC_I2S_CLK_GPIO_IDX, MIC_I2S_CLK_PIN_IDX, GPIO_MODE_I2S3);
  gpio_pin_set_mode(MIC_I2S_SD_GPIO_IDX, MIC_I2S_SD_PIN_IDX, GPIO_MODE_I2S3);
  gpio_pin_set_mode(MIC_I2S_WS_GPIO_IDX, MIC_I2S_WS_PIN_IDX, GPIO_MODE_I2S3);

  spi_rcc_cmd(MIC_I2S_NUMBER, ENABLE);

  const spi_hw_item_t *spi_hw_item = &spi_hw_items[MIC_I2S_NUMBER - 1];
  SPI_I2S_DeInit(spi_hw_item->spi);

  I2S_InitTypeDef i2s_init_struct;
  i2s_init_struct.I2S_Mode = I2S_Mode_MasterRx;
  i2s_init_struct.I2S_Standard = I2S_Standard_Phillips;
  i2s_init_struct.I2S_DataFormat = I2S_DataFormat_24b;
  i2s_init_struct.I2S_MCLKOutput = I2S_MCLKOutput_Disable;
  i2s_init_struct.I2S_AudioFreq = I2S_AudioFreq_44k;
  i2s_init_struct.I2S_CPOL = I2S_CPOL_Low;
  I2S_Init(spi_hw_item->spi, &i2s_init_struct);

  dma_rcc_cmd(MIC_I2S_DMA_NUMBER, ENABLE);

  const dma_steam_hw_item_t *dma_stream_hw_item =
      &dma_stream_hw_items[MIC_I2S_DMA_NUMBER - 1][MIC_I2S_DMA_STREAM_IDX];
  DMA_Cmd(dma_stream_hw_item->stream, DISABLE);
  DMA_DeInit(dma_stream_hw_item->stream);

  const uint32_t dma_channel = dma_channels[MIC_I2S_DMA_CHANNEL_IDX];

  DMA_InitTypeDef dma_init_struct;
  dma_init_struct.DMA_Channel = dma_channel;
  dma_init_struct.DMA_PeripheralBaseAddr = (uint32_t)&spi_hw_item->spi->DR;
  dma_init_struct.DMA_Memory0BaseAddr = (uint32_t)mis_i2s_buf;
  dma_init_struct.DMA_DIR = DMA_DIR_PeripheralToMemory;
  dma_init_struct.DMA_BufferSize = MIC_I2S_BUF_SIZE;
  dma_init_struct.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
  dma_init_struct.DMA_MemoryInc = DMA_MemoryInc_Enable;
  dma_init_struct.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
  dma_init_struct.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
  dma_init_struct.DMA_Mode = DMA_Mode_Circular;
  dma_init_struct.DMA_Priority = DMA_Priority_High;
  dma_init_struct.DMA_FIFOMode = DMA_FIFOMode_Enable;
  dma_init_struct.DMA_FIFOThreshold = DMA_FIFOThreshold_Full;
  dma_init_struct.DMA_MemoryBurst = DMA_MemoryBurst_Single;
  dma_init_struct.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
  DMA_Init(dma_stream_hw_item->stream, &dma_init_struct);

  DMA_Cmd(dma_stream_hw_item->stream, ENABLE);

  SPI_I2S_DMACmd(spi_hw_item->spi, SPI_I2S_DMAReq_Rx, ENABLE);
  I2S_Cmd(spi_hw_item->spi, ENABLE);

  while (1)
  {
    extern void tick_timer_ms_delay(uint32_t delay);
    tick_timer_ms_delay(100);
  }
}

#endif // #if (FEATURE_ENABLE_MIC)

/** @} */
