/**
 * @file led_cube.h
 * @brief
 * @author Vladimir Postnov
 * @date 07/06/2020
 * @addtogroup
 * @{
 */

#ifndef LED_CUBE_H_
#define LED_CUBE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "config.h"

#include <stdbool.h>

// ========================================= Definition ============================================

#define LED_CUBE_MAX_BRIGHTNESS                 50

typedef enum
{
  LED_CUBE_CELL_ON_MODE__OFF = 0,
  LED_CUBE_CELL_ON_MODE__ON,
  LED_CUBE_CELL_ON_MODE__AUTO_OFF,
} led_cube_cell_on_mode_t;

typedef struct
{
  uint8_t current;
  uint8_t target;
  uint8_t counter;
  enum
  {
    LED_CUBE_CELL_STATE__OFF = 0,
    LED_CUBE_CELL_STATE__ON_IN_PROG,
    LED_CUBE_CELL_STATE__OFF_IN_PROG,
  } state;
  led_cube_cell_on_mode_t mode;
} led_cube_cell_t;

typedef led_cube_cell_t led_cube_plane_t[LED_CUBE_Y_SIZE][LED_CUBE_Z_SIZE];

typedef struct
{
  uint8_t x_plane_counter;
  uint8_t brightness_counter;
  led_cube_cell_t cells[LED_CUBE_X_SIZE][LED_CUBE_Y_SIZE][LED_CUBE_Z_SIZE];
} led_cube_t;

// ========================================= Declaration ===========================================

void led_cube_init(led_cube_t *led_cube);
void led_cube_start_timer(led_cube_t *led_cube);
void led_cube_test(led_cube_t *led_cube);
void led_cube_set_cell(
    led_cube_t *led_cube,
    uint8_t x, uint8_t y, uint8_t z, uint8_t brightness,
    led_cube_cell_on_mode_t mode);
void led_cube_set_plane(led_cube_t *led_cube, uint8_t x, uint8_t b, led_cube_cell_on_mode_t mode);
bool led_cube_is_node_off(led_cube_t *led_cube, uint8_t x, uint8_t y, uint8_t z);
void led_cube_irq_handler(led_cube_t *led_cube);

#ifdef __cplusplus
}
#endif

#endif /* LED_CUBE_H_ */

/** @} */
