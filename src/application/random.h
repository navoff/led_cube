/**
 * @file random.h
 * @brief
 * @author Vladimir Postnov
 * @date 11/06/2020
 * @addtogroup
 * @{
 */

#ifndef RANDOM_H_
#define RANDOM_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "config.h"

// ========================================= Definition ============================================

// ========================================= Declaration ===========================================

void random_init(void);
uint32_t random_get_next(void);

#ifdef __cplusplus
}
#endif

#endif /* RANDOM_H_ */

/** @} */
