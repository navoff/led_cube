/**
 * @file mic.h
 * @brief
 * @author Vladimir Postnov
 * @date Aug 31, 2020
 * @addtogroup
 * @{
*/

#ifndef MIC_H_
#define MIC_H_

#ifdef __cplusplus
extern "C" {
#endif

// ========================================= Definition ============================================


// ========================================= Declaration ===========================================

void mic_init(void);

#ifdef __cplusplus
}
#endif

#endif /* MIC_H_ */

/** @} */
