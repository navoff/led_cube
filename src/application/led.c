/**
 * @file led.c
 * @brief
 * @author Vladimir Postnov
 * @date Jul 21, 2020
 * @addtogroup
 * @{
*/

#include "led.h"


// ========================================= Definition ============================================

// ========================================= Declaration ===========================================

// ======================================== Implementation =========================================

void led_init(void)
{
  gpio_rcc_cmd(LED_GPIO_IDX, ENABLE);
  gpio_pin_set_mode(LED_GPIO_IDX, LED_PIN_IDX, GPIO_MODE_OUT_PP);

  LED_OFF();
}

/** @} */
